#! /usr/bin/env python3

# Grau Anthony 11296038
# affichage.py - testé le 18/07/18
# affichage des résultats de SOM

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import operator
from pprint import pprint

hauteur = 0
largeur = 0
labels = []

def compute(nb_types, nb_list, index):
    """Calcule les poucentages de chaque type associé à un neurone.
    
Args:
    nb_types(int): Le nombre de types différents.
    nb_list(list): La liste avec le type de chaque entrée, le neurone associé et le nombre total d'entrées relevant de ce cas.
    index(int): Index du neurone.

Return:
    compteur(list): La liste des pourcentages de chaque type pour ce neurone.
    """
    
    compteur = [0.] * nb_types
    
    for elt in nb_list:
        if elt[1] == index:
            for types in labels:
                if types in elt[0]:
                    compteur[labels.index(types)] = elt[2]
                
    total = sum(compteur)
    if total:
        compteur = [round(val * 100 / total, 1) for val in compteur]
    
    return compteur
    
def som():
    """Affiche et sauvegarde la carte des diagrammes circulaires en sortie de SOM.

Args: -

Return: -

    """
    
    global labels
    
    to_work = []
    done = set()
    lab = set()
    nb_tup = []
    
    with open("effectifs.txt") as eff:
        brut = eff.readlines()
    to_work = [eval(elt[:-1]) for elt in brut if elt]
    for elt in to_work:
        if elt[0] not in lab:
            labels.append(elt[0])
            lab.add(elt[0])
        if elt not in done:
            nb_tup.append([elt[0], elt[1], to_work.count(elt)])
            done.add(elt)
    nb_tup = sorted(nb_tup, key=operator.itemgetter(1))
    
    plt.figure(num='Grille des effectifs')
    grille = GridSpec(hauteur, largeur)
    
    for i in range(hauteur):
        for j in range(largeur):
            fracs = compute(len(labels), nb_tup, i * largeur + j)
            if sum(fracs) != 0:
                plt.subplot(grille[i, j])
                plt.pie(fracs, shadow=True)
            else:
                plt.subplot(grille[i, j])
                plt.pie([100], colors=['black'], shadow=True)
                
    plt.legend(labels,loc=(-11,8), fontsize=8)
    
    plt.savefig('effectifs.png', bbox_inches='tight')

    plt.show()

def neurones():
    """Affiche et sauvegarde le profil des neurones sur la carte de SOM.

Args: -

Return: -

    """
    
    global hauteur
    global largeur
    
    to_work = []
    with open("connexions.txt") as co:
        brut = co.readlines()
    to_work = [eval(elt[:-1]) for elt in brut if elt]
    largeur = to_work[0][0]
    hauteur = to_work[0][1]
    to_work = to_work[1:]
    
    x = list(range(len(to_work[0])))
    
    plt.figure(num='Profils des neurones')
    grille = GridSpec(hauteur, largeur)
    
    for i in range(hauteur):
        for j in range(largeur):
            y = to_work[i * largeur + j]
            axes = plt.subplot(grille[i, j])
            axes.set_ylim(0, 1)
            axes.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
            if j != 0:
                axes.tick_params(axis='y', which='both', left=False, right=False, labelleft=False)
            plt.bar(x, y, width=1, color='b' )
    
    plt.savefig('neurones.png', bbox_inches='tight')
    
    plt.show()

if __name__ == '__main__':
    neurones()
    som()