/**
 * \file som.c
 * \brief Self-organizing map.
 * \author Anthony Grau 11296038 - mail: anthony[dot]grau[at]etud[dot]univ[dash]paris8[dot]fr
 * \date 18/07/2018
 * 
 * Implémentation de l'algorithme de self-organizing map.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <errno.h>
#include <stddef.h>

#define maximum 4096
#define MIN(a,b) (((a)<(b))?(a):(b))

const char * split_char = "," ;

typedef struct data_set { char * fleur ; double norme ; struct data_set * suite ; double valeurs[] ; } data_set, * all_data ;

all_data cons(long, size_t, char *,  all_data);
double calc_moy(all_data val, int indice, long nombre);
double aleaFloatBorne(double a, double b);
long calc_rayon(long taille_ligne_attracteur, long taille_colonne_attracteur);
double * fetch_val(long j, long taille_vect, all_data data_set);
long distance(long nbre, long taille, double * entree, double ** memoire);
long aleaInt(long max);
double d_node(long x_star, long y_star, long x_k, long y_k);
void putAll(long total, long taille_vect, all_data S, double ** memoire);
void usage(char *);

/**
 * \fn void putAll(long total, long taille_vect, all_data S, double ** memoire)
 * \brief Sauvegarde du type et du neurone le plus proche de chaque entrée.
 * 
 * \param total Le nombre total d'éléments de la carte finale.
 * \param taille_vect La dimension des données.
 * \param S La structure contenant les données en entrée.
 * \param memoire La matrice des neurones.
 * \return Cette fonction ne retourne rien.
 */
void putAll(long total, long taille_vect, all_data S, double ** memoire){
    
    long star;
    
    FILE * nb_to_star = fopen("effectifs.txt", "w");
    if(! nb_to_star) usage("Erreur d'ouverture du fichier effectifs.txt");
    
    while(S){
        char * tag;
        tag = S->fleur;
        fprintf(nb_to_star, "('%s',", tag) ;
        star = distance(total, taille_vect, S->valeurs, memoire);
        fprintf(nb_to_star, "%ld)\n", star);
        S = S->suite;
    }
    if(fclose(nb_to_star)) usage("Erreur de fermeture de effectifs.txt");
    return;
    
}

/**
 * \fn all_data cons(long taille_vect, size_t taille, char * ligne,  all_data struc_all)
 * \brief Construction de la structure contenant les données en entrée.
 * 
 * \param taille_vect La dimension des données.
 * \param taille La taille de la mémoire à réserver.
 * \param ligne La ligne de texte à convertir en données.
 * \param struct_all La structure contenant les données.
 * \return La structure à jour.
 */
all_data cons(long taille_vect, size_t taille, char * ligne,  all_data struc_all){
    all_data new = malloc(taille);
    if(!new) usage("Erreur d'allocation de la structure principale.");
    
    int i;
    double somme = 0.;
    char * mot = strtok(strdup(ligne), split_char) ;
    sscanf(mot, "%lf", &new->valeurs[0]);
    somme += pow(new->valeurs[0], 2);
    for(i= 0; i < taille_vect - 1; i++){
        mot = strtok(NULL, split_char) ;
        sscanf(mot, "%lf", &new->valeurs[i+1]);
        somme += pow(new->valeurs[i+1], 2);
    }
    new->norme = sqrt(somme);
    for(i=0; i < taille_vect; i++){
        new->valeurs[i] = new->valeurs[i] / new->norme;
    }
    new->fleur = strtok(NULL, split_char) ;
    new->suite = struc_all;
    
    return new ;
}

/**
 * \fn double * fetch_val(long j, long taille_vect, all_data data_set)
 * \brief Récupération du vecteur de valeurs associé à un indice dans la structure.
 * 
 * \param j La valeur de l'indice.
 * \param taille_vect La dimension des vecteurs.
 * \param data_set La structure contenant les données.
 * \return L'adresse du vecteur de données.
 */
double * fetch_val(long j, long taille_vect, all_data data_set){
    long i;
    long indice = 0;
    while (indice != j){
        indice +=1;
        data_set = data_set->suite;
    }
    double * val = malloc(taille_vect * sizeof(double));
    if(!val) usage("Erreur d'allocation pour la récupération du vecteur de valeurs.");
    for(i=0; i < taille_vect; i++){
        val[i] = data_set->valeurs[i];
    }
    
    return val;
}

/**
 * \fn double calc_moy(all_data val, int indice, long nombre)
 * \brief Calcule de la moyenne des valeurs associées à un indice donné.
 * 
 * \param val La structure de données.
 * \param indice la position indicée des valeurs à considérer.
 * \param nombre Le nombre total de valeurs en entrée de SOM.
 * \return La moyenne des valeurs d'un indice donné.
 */
double calc_moy(all_data val, int indice, long nombre){

    double moy_indice = 0;
    
    if (! val){
        usage("La liste est vide !");
    }
    
    while(val != NULL){
    moy_indice += val->valeurs[indice];
    val = val->suite;
    }
    
    
    return moy_indice / nombre;
}

/**
 * \fn double aleaFloatBorne(double a, double b)
 * \brief Générateur aléatoire de nombre.
 * 
 * \param a La borne inférieure.
 * \param b La borne supérieure.
 * \return Une valeur aléatoire entre a et b.
 */
double aleaFloatBorne(double a, double b){
    
    static int first = 0;
    
    if (first == 0){
        
        srand(time(NULL)); // initialisation de rand
        first = 1;
        
    }
    
    return (rand()/(double)RAND_MAX) * (b-a) + a;

}

/**
 * \fn long aleaInt(long max)
 * \brief Donne un nombre entier aléatoire entre 0 et un maximum (inclus).
 * 
 * \param max Le résultat maximum.
 * \return L'entier aléatoire.
 */
long aleaInt(long max){
    
    static int first = 0;
    
    if (first == 0){
        
        srand(time(NULL)); // initialisation de rand
        first = 1;
        
    }
    
    return (long)((rand() / (double)RAND_MAX * max) + 0.5); // arrondi au plus proche

}

/**
 * \fn long calc_rayon(long taille_ligne_attracteur, long taille_colonne_attracteur)
 * \brief Calcule n_init, le rayon d'influence de l'attracteur vainqueur.
 * 
 * \param taille_ligne_attracteur La taille d'une ligne sur la carte en sortie.
 * \param taille_colonne_attracteur La taille d'une colonne sur la carte en sortie.
 * \return La valeur retenue comme rayon.
 */
long calc_rayon(long taille_ligne_attracteur, long taille_colonne_attracteur){
    
    long nb = 1;
    long r = 0;
    long l = 1;
    long total = taille_ligne_attracteur * taille_colonne_attracteur;
    long hauteur, largeur;
    
    while (nb < 0.5 * (double) total){
        r += 1;
        l += 2;
        hauteur = MIN(taille_colonne_attracteur, l);
        largeur = MIN(taille_ligne_attracteur, l);
        nb = hauteur * largeur;
    }
    
    return r;
}

/**
 * \fn long distance(long nbre, long taille, double * entree, double ** memoire)
 * \brief Calcul de l'attracteur vainqueur avec choix aléatoire en cas d'ex aequo.
 * 
 * \param nbre Nombre de neurones.
 * \param taille Dimension des vecteurs.
 * \param entree Le vecteur en entrée.
 * \param memoire Le tableau des vecteurs mémoires connexions montantes.
 * \return L'indice du vecteur mémoire gagnant.
 */
long distance(long nbre, long taille, double * entree, double ** memoire){
    long i, j, nbre_gagnant;
    long gagnant, alea;
    double somme;
    double * distances_tableau = malloc(nbre * sizeof(double));
    if(! distances_tableau) usage("Erreur d'allocation du tableau de distances.");
    double * candidats = malloc(nbre * sizeof(double));
    if(! candidats) usage("Erreur d'allocation du tableau des candidats.");

    for(j=0; j < nbre; j++){
        somme = 0.;
        for(i = 0; i < taille; i++){
            somme += pow(entree[i] - memoire[j][i + 1], 2);
        }
        distances_tableau[j] = sqrt(somme);
    }
    gagnant = 0;
    nbre_gagnant = 0;
    for(j = 1; j < nbre; j++){
        if(distances_tableau[j] < distances_tableau[gagnant])
            gagnant = j;
    }
    for (j = 0; j < nbre; j++){
        if(distances_tableau[j] == distances_tableau[gagnant]){
            candidats[nbre_gagnant] = j;
            nbre_gagnant += 1;
        }
    }
    if(nbre_gagnant > 1){
        alea = aleaInt(nbre_gagnant - 1);
        gagnant = candidats[alea];
    }
    
    free(distances_tableau);
    free(candidats);
    
    return gagnant;
}

/**
 * \fn double d_node(long x_star, long y_star, long x_k, long y_k)
 * \brief Calcule la distance euclidienne entre un vecteur et l'attracteur vainqueur.
 * 
 * \param x_star l'abscisse de l'attracteur vainqueur.
 * \param y_star l'ordonnée de l'attracteur vainqueur.
 * \param x_k l'abscisse du vecteur en entrée.
 * \param y_k l'ordonnée du vecteur en entrée.
 * \return La distance euclidienne entre les deux vecteurs.
 */
double d_node(long x_star, long y_star, long x_k, long y_k){
    
    double somme = 0.;
    double result;
    
    somme +=  pow(x_star - x_k, 2);
    somme +=  pow(y_star - y_k, 2);
    
    result = sqrt(somme);
    
    return result;
}

/**
 * \fn void usage(char *message)
 * \brief Affiche un message d'erreur et renvoie un code de sortie 1.
 * 
 * \param message La chaine à afficher.
 * \return 1
 */
void usage(char *message){
    
    fprintf(stderr, "%s\n", message) ;
    exit(1) ;
    
}

/**
 * \fn int main(int argc, char * argv[])
 * \brief Entrée du programme.
 * 
 * \param argc Le nombre d'arguments sur la ligne de commande (un argument).
 * \param argv Le tableau des arguments. Le nom du fichier.
 * \return 0
 */
int main(int argc, char * argv[]){
    
    //Contrôle de l'argument
    if (argc != 3) usage("Deux arguments: la taille des vecteurs et le nom du fichier.") ;
    
    //Initialisation des variables
    long nombre = 0;
    long i, j, k, h;
    long star;
    double alpha_0;
    double alpha;
    long n_init;
    double n_size;
    long x_star, y_star, x_k, y_k;
    double to_star;
    
    char * ptr;
    errno = 0;
    long taille_vect = strtol(argv[1], &ptr, 10);
    if (errno != 0 || *ptr != '\0' || taille_vect > INT_MAX) {
        usage("Votre argument n'est pas valide. Erreur taille vecteurs.");
    }
    if (taille_vect < 2) usage("La taille des vecteurs doit être supérieure ou égale à 2.") ;
    char ligne[maximum] ;
    size_t taille = offsetof(struct data_set, valeurs) + sizeof(double) * taille_vect ;
    long total = 0;
    long taille_ligne_attracteur = 0;
    long taille_colonne_attracteur = 0;
    long nbre_max = taille_vect * 500;
    long iter1 = (long int) (floor(0.25 * (double) nbre_max + 0.5));
    long iter2 = nbre_max - iter1;
    
    //Ouverture du fichier argument
    FILE * flux = fopen(argv[2], "r") ;
    if (! flux) usage("Erreur d'ouverture du fichier de données en entrée.") ;
    
    //Construction de la structure à partir du fichier ouvert
    all_data data_set = NULL;
    while (strtok(fgets(ligne, maximum, flux), "\n")){
        nombre += 1;
        data_set = cons(taille_vect, taille, ligne, data_set) ;
    }
    
    //putAll(data_set);
    
    //Initialisation des attracteurs
    double * moy = malloc(taille_vect * sizeof(double));
    if(!moy) usage("Erreur d'allocation du tableau des moyennes.");
    
    for(i=0; i < taille_vect; i++){
        moy[i] = calc_moy(data_set, i, nombre);
    }
    
    total = (long int) (floor(5 * sqrt(nombre) + 0.5));
    taille_ligne_attracteur = (long int) ceil(sqrt(total));
    taille_colonne_attracteur = (long int) floor(sqrt(total));
    
    while(taille_ligne_attracteur * taille_colonne_attracteur < total){
        taille_ligne_attracteur += 1;
    }
    
    total = taille_ligne_attracteur * taille_colonne_attracteur;
    
    double **vect_connexions_montantes;
    vect_connexions_montantes = malloc(total * sizeof(double*));
    if(!vect_connexions_montantes) usage("Erreur d'allocation du vecteur des connexions montantes.");
    for(i=0; i < total; i++){
        vect_connexions_montantes[i] = malloc((taille_vect + 1) * sizeof(double));
        if(!vect_connexions_montantes[i]) usage("Erreur allocation dimension connexions montantes.");
    }
    
    double limInf = 0.;
    double limSup = 1.;
    
    for(i=0; i < total; i++){
        double somme = 0.;
        for(j=1; j < (taille_vect + 1); j++){
            limInf = moy[j - 1] - 0.2;
            if (limInf < 0) limInf = 0;
            limSup = moy[j - 1] + 0.2;
            if (limSup > 1) limSup = 1;
            vect_connexions_montantes[i][j] = aleaFloatBorne(limInf, limSup);
            somme += pow(vect_connexions_montantes[i][j], 2);
        }
        vect_connexions_montantes[i][0] = sqrt(somme);
    }
    
    for(i=0; i < total; i++){
        for(j=1; j < taille_vect + 1; j++){
            vect_connexions_montantes[i][j] = vect_connexions_montantes[i][j] / vect_connexions_montantes[i][0];
        }
    }
    
    //Apprentissage: première phase
    alpha_0 = 0.6;
    n_init = calc_rayon(taille_ligne_attracteur, taille_colonne_attracteur);
    n_size = (double) n_init;
    alpha = alpha_0;
    
    for(i=0; i < iter1; i++){
        
        alpha = alpha_0 * (1 - (i / (iter1 - 1)));
        n_size = ((double) n_init) * (1 - (i / (iter1 - 1)));
        
        for(j=0; j < nombre; j++){
            double * vect_de_donnees = malloc(taille_vect * sizeof(double));
            if(! vect_de_donnees) usage("Erreur d'allocation du vecteur de données copie 1.");
            vect_de_donnees = fetch_val(j, taille_vect, data_set);
            star = distance(total, taille_vect, vect_de_donnees, vect_connexions_montantes);
            x_star = star % taille_ligne_attracteur;
            y_star = star / taille_ligne_attracteur;
            for(k=0; k < total; k++){
                x_k = k % taille_ligne_attracteur;
                y_k = k / taille_ligne_attracteur;
                if ((abs(x_star - x_k) <= n_size) && (abs(y_star - y_k) <= n_size)){
                    to_star = d_node(x_star, y_star, x_k, y_k);
                    for(h=0; h < taille_vect; h++){
                        vect_connexions_montantes[k][h + 1] += (alpha / (1 + to_star)) * (vect_de_donnees[h] - vect_connexions_montantes[k][h + 1]);
                    }
                }
            }
            free(vect_de_donnees);
        }
    }
    
    //Apprentissage: seconde phase
    alpha_0 = 0.9;
    n_init = calc_rayon(taille_ligne_attracteur, taille_colonne_attracteur);
    n_size = (double) n_init;
    alpha = alpha_0;
    
    for(i=0; i < iter2; i++){
        
        alpha = alpha_0 * (1 - (i / (iter2 - 1)));
        n_size = ((double) n_init) * (1 - (i / (iter2 - 1)));
        
        for(j=0; j < nombre; j++){
            double * vect_de_donnees = malloc(taille_vect * sizeof(double));
            if(! vect_de_donnees) usage("Erreur d'allocation du vecteur de données copie 2.");
            vect_de_donnees = fetch_val(j, taille_vect, data_set);
            star = distance(total, taille_vect, vect_de_donnees, vect_connexions_montantes);
            x_star = star % taille_ligne_attracteur;
            y_star = star / taille_ligne_attracteur;
            for(k=0; k < total; k++){
                x_k = k % taille_ligne_attracteur;
                y_k = k / taille_ligne_attracteur;
                if ((abs(x_star - x_k) <= n_size) && (abs(y_star - y_k) <= n_size)){
                    to_star = d_node(x_star, y_star, x_k, y_k);
                    for(h=0; h < taille_vect; h++){
                        vect_connexions_montantes[k][h + 1] += (alpha / (1 + to_star)) * (vect_de_donnees[h] - vect_connexions_montantes[k][h + 1]);
                    }
                }
            }
            free(vect_de_donnees);
        }
    }
    
    //Ecriture des fichiers nécessaires à la présentation
    
    FILE * to_save = fopen("connexions.txt", "w");
    if(! to_save) usage("Erreur d'ouverture du fichier connexions.txt");
    fprintf(to_save, "[%ld, %ld]\n", taille_ligne_attracteur, taille_colonne_attracteur);
    
    for(i=0; i < total; i++){
        fprintf(to_save, "[");
        for(j=1; j < taille_vect + 1 ; j++){
            fprintf(to_save, "%.4lf,", vect_connexions_montantes[i][j]);
        }
        fprintf(to_save, "]\n");
    }
    
    for(j=0; j < nombre; j++){
        putAll(total, taille_vect, data_set, vect_connexions_montantes);
    }

    //Fermeture du flux et libération mémoire
    all_data tmp = data_set;
    all_data tmp_nxt;
    while(tmp != NULL){
        tmp_nxt = tmp->suite;
        free(tmp);
        tmp = tmp_nxt;
    }
    free(moy);
    free(vect_connexions_montantes);
    
    if(fclose(flux)) usage("Erreur de fermeture de données en entrée.");
    if(fclose(to_save)) usage("Erreur de fermeture de connexions.txt");
    return 0 ;
    
}